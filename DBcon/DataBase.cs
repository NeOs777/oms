﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Text;
using System.Data;
using System.Data.Common;
using System.IO;

namespace DBcon
{
    public class DataBase
    {
        private string _dbname;
        private SQLiteConnection _connection;
        private SQLiteCommand _command;
        private SQLiteDataAdapter _dataAdapter;

        private DataSet _ds;
        public DataSet DS
        {
            get { return _ds; }
            set { _ds = value; }
        }

        public DataBase(string nDataBase)
        {
            _dbname = nDataBase;
            if (!File.Exists(_dbname))
            {
                SQLiteConnection.CreateFile(_dbname);
            }
            string comdb = "Data Source="+_dbname+";Version=3;Compress=True;";
            _connection = new SQLiteConnection(comdb);
        }

        public void ExecuteQuery(string Query)
        {
            _connection.Open();
            _command = _connection.CreateCommand();
            _command.CommandText = Query;
            _command.ExecuteNonQuery();
            _connection.Close();
        }

        public void LoadData(string tablename)
        {
            _connection.Open();
            _command = _connection.CreateCommand();
            string CommandText = "SELECT * FROM " + tablename + ";";
            _dataAdapter = new SQLiteDataAdapter(CommandText, _connection);
            _ds.Reset();
            _dataAdapter.Fill(_ds);
            _connection.Close();
        }
    }
}
