﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using System.IO;

namespace Store
{
    /// <summary>
    /// Этот класс продукции в магазине.
    /// </summary>
    public class Product
    {
        private Mobile _firstMobile;

        public Mobile FirstMobile
        {
            get { return _firstMobile; }
            set { _firstMobile = value; }
        }
        private string _name;
        public string PName
        {
            get { return _name; }
            set { _name = value; }
        }

        private Int32 _allprice = 0;
        public Int32 PAllprice
        {
            get { return _allprice; }
            set { _allprice = value; }
        }

        public Product(String aname)
        {
            _name = aname;
            _firstMobile = new Mobile(null, null, null, null, 0);
            _firstMobile.DNext = null;
        }

        public void Add(Mobile atemp,string sename)
        {
            _allprice += atemp.Price;
            if (_firstMobile.PName==null)
            {
                atemp.DNext = _firstMobile;
                _firstMobile = atemp;
            }
            else
            {
                atemp.DNext = _firstMobile;
                Mobile temp = Search(sename);
                if (temp.DNext == _firstMobile)
                {
                    temp.DNext = atemp;
                }
                else
                {
                    atemp.DNext = temp.DNext;
                    temp.DNext = atemp;
                }
                
            }
        }
        public bool Delete(string mname)
        {
            Mobile Temp;
            Temp = _firstMobile;
            bool se = false;
            while (Temp.DNext != null && (Temp.DNext != _firstMobile))
            {

                if (Temp.PName == mname)
                {
                    se = true;
                    break;
                }
                else
                {
                    Temp = Temp.DNext;

                }
            }
            if (Temp.DNext.DNext != _firstMobile) Temp.DNext = Temp.DNext.DNext;
            else Temp.DNext = _firstMobile;

            if (se)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public Mobile Search(string atemp)
        {
            Mobile Temp = FirstMobile;
            bool se = false;
            while (Temp != null && (Temp.DNext != _firstMobile))
            {

                if (Temp.PName == atemp)
                {
                    
                    se = true;
                    break;
                }
                else
                {
                    
                        Temp = Temp.DNext;  
                   
                      
                    

                }
            }

            if (se) return Temp;
            else return null;
        }

        public bool Edit(Mobile atemp,string aset)
        {
            Mobile temp = Search(aset);

            if (atemp.PDisplay != temp.PDisplay)
            {
                temp.PDisplay = atemp.PDisplay;
            }

            if (atemp.PName != temp.PName)
            {
                temp.PName = atemp.PName;
            }

            if (atemp.PBattery != temp.PBattery)
            {
                temp.PBattery = atemp.PBattery;
            }

            if (atemp.PMemory != temp.PMemory)
            {
                temp.PMemory = atemp.PMemory;
            }
            temp.Price = atemp.Price;
            return true;
        }

        public void Save(string filename)
        {
            Mobile temp = FirstMobile;
            StreamWriter writer = new StreamWriter(filename);
            while (temp.DNext != null && temp.DNext != FirstMobile)
            {
                writer.WriteLine(temp.PName);
                writer.WriteLine(temp.PBattery.PName);
                writer.WriteLine(temp.PBattery.Price);
                writer.WriteLine(temp.PDisplay.PName);
                writer.WriteLine(temp.PDisplay.Price);
                writer.WriteLine(temp.PMemory.PName);
                writer.WriteLine(temp.PMemory.Price);
                writer.WriteLine(temp.PMemory.ROM);
                writer.WriteLine(temp.Price);
                temp = temp.DNext;
            }
            writer.Close();
        }

        public void Load(string filename)
        {
            Mobile temp = _firstMobile;
            string prevname;
            while (temp.DNext != null && temp.DNext != FirstMobile)
            {
                temp = temp.DNext;
            }
            prevname = temp.PName;
            StreamReader reader = new StreamReader(filename);
            try
            {
                do
                {

                    string tname;
                    tname = reader.ReadLine();
                    string bname;
                    bname = reader.ReadLine();
                    Int32 bprise;
                    bprise = Convert.ToInt32(reader.ReadLine());
                    string dname;
                    dname = reader.ReadLine();
                    Int32 dprise;
                    dprise = Convert.ToInt32(reader.ReadLine());
                    string mname;
                    mname = reader.ReadLine();
                    Int32 mprise;
                    mprise = Convert.ToInt32(reader.ReadLine());
                    Int32 mrom;
                    mrom = Convert.ToInt32(reader.ReadLine());
                    Int32 pprice;
                    pprice = Convert.ToInt32(reader.ReadLine());
                    Battery battery = new Battery(bname, bprise);
                    Display disp = new Display(dname, dprise);
                    Memory mem = new Memory(mname, mprise, mrom);
                    Mobile Mobile = new Mobile(tname, disp, battery, mem, pprice);
                    Add(Mobile, prevname);
                    prevname = tname;
                }
                while (reader.Peek() != -1);

            }

            finally
            {
                reader.Close();
            }
        }
        
    }
}
