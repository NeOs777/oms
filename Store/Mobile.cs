﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Store
{
    /// <summary>
    /// Класс мобильных телефонов.
    /// </summary>
    public class Mobile
    {
        private Display _display;
        public Display PDisplay
        {
            get { return _display; }
            set { _display = value; }
        }

        private Battery _battery;
        public Battery PBattery
        {
            get { return _battery; }
            set { _battery = value; }
        }

        private Memory _memory;
        public Memory PMemory
        {
            get { return _memory; }
            set { _memory = value; }
        }

        private string _name;
        public string PName
        {
            get { return _name; }
            set { _name = value; }
        }
        private Mobile _next;
        public Mobile DNext
        {
            get { return _next; }
            set { _next = value; }
        }
        private Int32 _price;
        public Int32 Price
        {
            get { return _price; }
            set { _price = value; }
        }

        public Mobile(String aname, Display aDisplay, Battery aBattery,Memory aMemory, Int32 aPrice)
        {
            _name = aname;
            _display = aDisplay;
            _battery = aBattery;
            _memory = aMemory;
            _price = aPrice;
        }
        
        

    }

    /// <summary>
    /// Класс дисплеев.
    /// </summary>
    public class Display
    {
        private Int32 _price;
        public Int32 Price
        {
            get { return _price; }
            set { _price = value; }
        }
        private string _name;
        public string PName
        {
            get { return _name; }
            set { _name = value; }
        }
       public Display(string aName, Int32 aprice)
        {
            _name = aName;
            _price = aprice;
        }
    }

    /// <summary>
    /// Класс батареи.
    /// </summary>
    public class Battery
    {
        private Int32 _price;
        public Int32 Price
        {
            get { return _price; }
            set { _price = value; }
        }
        private string _name;
        public string PName
        {
            get { return _name; }
            set { _name = value; }
        }

        public Battery(string aName, Int32 aprice)
        {
            _name = aName;
            _price = aprice;
        }
    }

    /// <summary>
    /// Класс памяти.
    /// </summary>
    public class Memory
    {
        private string _name;
        public string PName
        {
            get { return _name; }
            set { _name = value; }
        } 
        private Int32 _rom;
        public Int32 ROM
        {
            get { return _rom; }
            set { _rom = value; }
        }

        private Int32 _price;
        public Int32 Price
        {
            get { return _price; }
            set { _price = value; }
        }

        public Memory(string aName, Int32 aprice, Int32 arom)
        {
            _name = aName;
            _price = aprice;
            _rom = arom;
        }
    }
}
