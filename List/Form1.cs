﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Store;

namespace List
{
    public partial class Form1 : Form
    {
        public string NameComp;
        public Product Product;
        public Form1()
        {
            nm secondForm = new nm(this);
            secondForm.ShowDialog(this);
            Product=new Product(NameComp);
            
            InitializeComponent();
            label1.Text +="«"+ NameComp +"»";
        }

        public void Updatelist()
        {
            listBox1.Items.Clear();
            Mobile temp = Product.FirstMobile;
            while (temp.DNext != null && temp.DNext != Product.FirstMobile)
            {
                listBox1.Items.Add(temp.PName);
                temp = temp.DNext;
            }
        }

        private void Print(string sname)
        {
            Mobile temp = Product.Search(sname);
            nlab.Text = temp.PName;
            batlab.Text = temp.PBattery.PName;
            displab.Text = temp.PDisplay.PName;
            nromlab.Text = temp.PMemory.PName;
            romlab.Text = temp.PMemory.ROM.ToString();
            pricelab.Text = temp.Price.ToString();
            del.Visible = true;
            edit.Visible = true;
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string sname = listBox1.SelectedItem.ToString();
            Print(sname);
        }

        private void добавитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Add secondForm = new Add(this);
            secondForm.ShowDialog(this);
            Updatelist();
            
        }

        private void открытьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.InitialDirectory = Application.ExecutablePath;
            ofd.DefaultExt = "filter";
            ofd.Filter = "Database (*.mzh) |*.mzh";
            ofd.FilterIndex = 1;
            ofd.AddExtension = true;
            ofd.RestoreDirectory = true;
            ofd.Title = "Открыть";
            //Если результат равен ОК - пользователь нажал кнопку "Открыть", то ..
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                //try
                //{
                        String fileName = ofd.FileName;
                        Product.Load(fileName);
                    
                //}
                //catch (Exception ex)
                //{
                //    MessageBox.Show("Error: Could not read file from disk. Original error: " + ex.Message);
                //}
            }
                // Сохраняем весь текстовый файл в файле на диске
              Updatelist();  
            }

        private void сохранитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.InitialDirectory = Application.ExecutablePath;
            sfd.DefaultExt = "filter";
            sfd.Filter = "Database (*.mzh) |*.mzh";
            sfd.FilterIndex = 1;
            sfd.AddExtension = true;
            sfd.CheckFileExists = false;
            sfd.Title = "Сохранить в";
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                String fileName = sfd.FileName;

                Product.Save(fileName);
            }

        }

        private void закрытьToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void del_Click(object sender, EventArgs e)
        {
            bool temp = Product.Delete(nlab.Text);
            Updatelist();
            if (temp)
            {
                DialogResult result = MessageBox.Show("Модель удлена", "Удалено", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                DialogResult result = MessageBox.Show("Модель не удлена", "Не удалено", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            del.Visible = false;
            edit.Visible = false;
        }

        

        private void sebut_Click(object sender, EventArgs e)
        {
            Mobile temp = Product.Search(setb.Text);
            if (temp != null)
            {
                Print(temp.PName);
            }

            else
            {
                DialogResult result = MessageBox.Show("Модель не найдена", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }

        private void edit_Click(object sender, EventArgs e)
        {
            Edit secondForm = new Edit(this);
            secondForm.ShowDialog(this);
            Updatelist();
        }
        


    }


}
