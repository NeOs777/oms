﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Store;

namespace List
{
    public partial class Add : Form
    {
        private Form1 frm;
        public Add(Form1 f)
        {
            InitializeComponent();
            frm = f;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Battery tempBattery = new Battery(namebat.Text,Convert.ToInt32(pricebat.Text));
            Display tempDisplay = new Display(namedisp.Text, Convert.ToInt32(pricedisp.Text));
            Memory tempMemory = new Memory(namerom.Text, Convert.ToInt32(pricerom.Text), Convert.ToInt32(rom.Text));
            Int32 price = tempBattery.Price + tempDisplay.Price + tempMemory.Price;
            Mobile temp = new Mobile(nameprod.Text,tempDisplay,tempBattery,tempMemory,price);
            frm.Product.Add(temp,textBox1.Text);
            frm.listBox1.Items.Add(nameprod.Text);
            this.Close();
            
        }

        
    }
}
