﻿namespace List
{
    partial class Edit
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.romlab = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.pricerom = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.nromlab = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.pricebat = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.batlab = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.pricedisp = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.displab = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.nlab = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.setb = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.button1.Location = new System.Drawing.Point(255, 358);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(186, 34);
            this.button1.TabIndex = 1410;
            this.button1.Text = "Изменить";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // romlab
            // 
            this.romlab.Location = new System.Drawing.Point(520, 318);
            this.romlab.Name = "romlab";
            this.romlab.Size = new System.Drawing.Size(153, 20);
            this.romlab.TabIndex = 1409;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label12.Location = new System.Drawing.Point(453, 319);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(61, 16);
            this.label12.TabIndex = 1415;
            this.label12.Text = "Размер:";
            // 
            // pricerom
            // 
            this.pricerom.Location = new System.Drawing.Point(302, 318);
            this.pricerom.Name = "pricerom";
            this.pricerom.Size = new System.Drawing.Size(134, 20);
            this.pricerom.TabIndex = 1408;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label9.Location = new System.Drawing.Point(252, 319);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(44, 16);
            this.label9.TabIndex = 1414;
            this.label9.Text = "Цена:";
            // 
            // nromlab
            // 
            this.nromlab.Location = new System.Drawing.Point(99, 318);
            this.nromlab.Name = "nromlab";
            this.nromlab.Size = new System.Drawing.Size(135, 20);
            this.nromlab.TabIndex = 1407;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label10.Location = new System.Drawing.Point(16, 319);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(77, 16);
            this.label10.TabIndex = 1421;
            this.label10.Text = "Название:";
            // 
            // label11
            // 
            this.label11.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label11.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label11.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label11.Location = new System.Drawing.Point(-1, 271);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(685, 36);
            this.label11.TabIndex = 1411;
            this.label11.Text = "Память";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pricebat
            // 
            this.pricebat.Location = new System.Drawing.Point(447, 237);
            this.pricebat.Name = "pricebat";
            this.pricebat.Size = new System.Drawing.Size(226, 20);
            this.pricebat.TabIndex = 1406;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(397, 238);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(44, 16);
            this.label6.TabIndex = 1420;
            this.label6.Text = "Цена:";
            // 
            // batlab
            // 
            this.batlab.Location = new System.Drawing.Point(99, 236);
            this.batlab.Name = "batlab";
            this.batlab.Size = new System.Drawing.Size(284, 20);
            this.batlab.TabIndex = 1405;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.Location = new System.Drawing.Point(16, 237);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(77, 16);
            this.label7.TabIndex = 1419;
            this.label7.Text = "Название:";
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label8.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label8.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label8.Location = new System.Drawing.Point(0, 189);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(685, 36);
            this.label8.TabIndex = 1418;
            this.label8.Text = "Батарея";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pricedisp
            // 
            this.pricedisp.Location = new System.Drawing.Point(447, 159);
            this.pricedisp.Name = "pricedisp";
            this.pricedisp.Size = new System.Drawing.Size(226, 20);
            this.pricedisp.TabIndex = 1404;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(397, 160);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(44, 16);
            this.label5.TabIndex = 1417;
            this.label5.Text = "Цена:";
            // 
            // displab
            // 
            this.displab.Location = new System.Drawing.Point(99, 158);
            this.displab.Name = "displab";
            this.displab.Size = new System.Drawing.Size(284, 20);
            this.displab.TabIndex = 1403;
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label3.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label3.Location = new System.Drawing.Point(-1, 111);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(685, 36);
            this.label3.TabIndex = 1412;
            this.label3.Text = "Дисплей";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // nlab
            // 
            this.nlab.Location = new System.Drawing.Point(99, 80);
            this.nlab.Name = "nlab";
            this.nlab.Size = new System.Drawing.Size(574, 20);
            this.nlab.TabIndex = 1402;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(16, 81);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 16);
            this.label2.TabIndex = 1413;
            this.label2.Text = "Название:";
            // 
            // setb
            // 
            this.setb.Location = new System.Drawing.Point(233, 37);
            this.setb.Name = "setb";
            this.setb.Size = new System.Drawing.Size(131, 20);
            this.setb.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(16, 159);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(77, 16);
            this.label4.TabIndex = 1416;
            this.label4.Text = "Название:";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.WindowFrame;
            this.panel1.Controls.Add(this.button2);
            this.panel1.Controls.Add(this.setb);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(1, 1);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(685, 70);
            this.panel1.TabIndex = 1401;
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label1.Location = new System.Drawing.Point(-1, -1);
            this.label1.Name = "label1";
            this.label1.Padding = new System.Windows.Forms.Padding(0, 10, 0, 0);
            this.label1.Size = new System.Drawing.Size(685, 70);
            this.label1.TabIndex = 100;
            this.label1.Text = "Изменение товара";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(370, 35);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 101;
            this.button2.Text = "Поиск";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // Edit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(684, 405);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.romlab);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.pricerom);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.nromlab);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.pricebat);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.batlab);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.pricedisp);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.displab);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.nlab);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.panel1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Edit";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.Text = "Изменение";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox romlab;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox pricerom;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox nromlab;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox pricebat;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox batlab;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox pricedisp;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox displab;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox nlab;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox setb;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button2;
    }
}