﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Store;

namespace List
{
    public partial class Edit : Form
    {
        private string from;
        private Form1 frm;
        public Edit(Form1 f)
        {
            InitializeComponent();
            frm = f;
            from = frm.nlab.Text;
            Print(from);
        }

        private Mobile temp;
        private void Print(string sname)
        {
            temp = frm.Product.Search(sname);
            nlab.Text = temp.PName;
            setb.Text = temp.PName;
            batlab.Text = temp.PBattery.PName;
            displab.Text = temp.PDisplay.PName;
            nromlab.Text = temp.PMemory.PName;
            romlab.Text = temp.PMemory.ROM.ToString();
            pricedisp.Text = temp.PDisplay.Price.ToString();
            pricebat.Text = temp.PBattery.Price.ToString();
            pricerom.Text = temp.PMemory.Price.ToString();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            temp = frm.Product.Search(setb.Text);
            if (temp != null)
            {
                Print(temp.PName);
                from = temp.PName;
            }

            else
            {
                DialogResult result = MessageBox.Show("Модель не найдена", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Battery battemp;
            Display disptemp;
            Memory memtemp;
            if (temp.PBattery.PName != batlab.Text || temp.PBattery.Price.ToString() != pricebat.Text)
            {
                 battemp = new Battery(batlab.Text, Convert.ToInt32(pricebat.Text));
            }
            else
            {
                 battemp = temp.PBattery;
            }
            if (temp.PDisplay.PName != displab.Text || temp.PDisplay.Price.ToString() != pricedisp.Text)
            {
                 disptemp = new Display(displab.Text, Convert.ToInt32(pricedisp.Text));
            }
            else
            {
                 disptemp = temp.PDisplay;
            }
            if (temp.PMemory.PName != nromlab.Text || temp.PMemory.Price.ToString() != pricerom.Text || temp.PMemory.ROM.ToString() != romlab.Text)
            {
                 memtemp = new Memory(nromlab.Text, Convert.ToInt32(pricerom.Text), Convert.ToInt32(romlab.Text));
            }
            else
            {
                 memtemp = temp.PMemory;
            }
            Int32 price = battemp.Price + disptemp.Price + memtemp.Price;
            Mobile atemp = new Mobile(nlab.Text, disptemp, battemp, memtemp, price);
            bool Ed = frm.Product.Edit(atemp, from);
            this.Close();
        }
    }
}
